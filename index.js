const express = require('express');
//db
const categoriesDb = require('./fs/categoriesFs');
const locationsDb = require('./fs/locationsFs');
const accountingSubjectsDb = require('./fs/accountingSubjectsFs');
//rout
const categories = require('./app/categories');
const locations = require('./app/locations');
const accountingSubjects = require('./app/accountingSubjects');
//cors
const cors = require("cors");

const app = express();
app.use(express.static('public'))
app.use(express.json());
app.use(cors());


categoriesDb.init();
locationsDb.init();
accountingSubjectsDb.init();

const port = 8000;

app.use('/categories', categories);
app.use('/locations', locations);
app.use('/accountingSubjects', accountingSubjects);

app.listen(port, () => {
    console.log(`server port${port}`)
});