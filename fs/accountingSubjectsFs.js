const accountingSubjectsFs = require('fs');
const {nanoid} = require("nanoid");

const fileName = './accountingSubjectsDb.json';
let data = [];

module.exports = {
    init() {
        try {
            const fileContents = accountingSubjectsFs.readFileSync(fileName);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        const result = data.map(accountingSubject => {
            const copy = {...accountingSubject};
            delete copy.description;
            return copy;
        });
        return result;
    },

    getItemById(id) {
        return data.find(item => item.id === id);
    },

    deleteItemById(id) {
        const accountingSubjectToRemove = data.find(item => item.id === id);
        if(accountingSubjectToRemove) {
            const accountingSubjectIndex = data.indexOf(accountingSubjectToRemove);
            data.splice(accountingSubjectIndex,1);
            this.save();

            return 'deleted';
        } else {
            return 'error';
        }
    },

    addItem(item) {
        item.id = nanoid();
        data.push(item);
        this.save();
        return item;
    },

    save() {
        accountingSubjectsFs.writeFileSync(fileName, JSON.stringify(data,null, 2));
    }


}