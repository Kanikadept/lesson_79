const locationsFs = require('fs');
const {nanoid} = require("nanoid");

const fileName = './locationsDb.json';
const accSubFileName = './accountingSubjectsDb.json';
let data = [];
let accountingSubjectsData = [];

module.exports = {
    init() {
        try {
            const fileContents = locationsFs.readFileSync(fileName);
            data = JSON.parse(fileContents);

            const accSubFileContents = locationsFs.readFileSync(accSubFileName);
            accountingSubjectsData = JSON.parse(accSubFileContents);
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        const result = data.map(location => {
            const copy = {...location};
            delete copy.description;
            return copy;
        });
        return result;
    },

    getItemById(id) {
        return data.find(item => item.id === id);
    },

    deleteItemById(id) {
        const locationToRemove = data.find(item => item.id === id);
        if(locationToRemove) {
            const result = accountingSubjectsData.find(item => item.locationId === locationToRemove.id);
            if(result) {
                return 'foreign key error';
            }

            const locationIndex = data.indexOf(locationToRemove);
            data.splice(locationIndex,1);
            this.save();

            return 'deleted';
        } else {
            return 'error';
        }
    },

    addItem(item) {
        item.id = nanoid();
        data.push(item);
        this.save();
        return item;
    },

    save() {
        locationsFs.writeFileSync(fileName, JSON.stringify(data,null, 2));
    }


}