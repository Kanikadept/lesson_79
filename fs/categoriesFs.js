const categoriesFs = require('fs');
const {nanoid} = require("nanoid");

const fileName = './categoriesDb.json';
const accSubFileName = './accountingSubjectsDb.json';

let data = [];
let accountingSubjectsData = [];

module.exports = {
    init() {
        try {
            const fileContents = categoriesFs.readFileSync(fileName);
            data = JSON.parse(fileContents);

            const accSubFileContents = categoriesFs.readFileSync(accSubFileName);
            accountingSubjectsData = JSON.parse(accSubFileContents);
        } catch (e) {
            data = [];
        }
    },

    getItems() {
         const result = data.map(category => {
            const copy = {...category};
            delete copy.description;
            return copy;
        });
         return result;
    },

    getItemById(id) {
        return data.find(item => item.id === id);
    },

    deleteItemById(id) {
        const categoryToRemove = data.find(item => item.id === id);
        if(categoryToRemove) {
            const result = accountingSubjectsData.find(item => item.categoryId === categoryToRemove.id);
            if(result) {
                return 'foreign key error';
            }

            const categoryIndex = data.indexOf(categoryToRemove);
            data.splice(categoryIndex,1);
            this.save();

            return 'deleted';
        } else {
            return 'error';
        }
    },

    addItem(item) {
        item.id = nanoid();
        data.push(item);
        this.save();
        return item;
    },

    save() {
        categoriesFs.writeFileSync(fileName, JSON.stringify(data,null, 2));
    }


}