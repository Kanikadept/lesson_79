const express = require('express');
const categoriesDb = require('../fs/categoriesFs')

const router = express.Router();


router.get('/', (req, res) => {

    const categories = categoriesDb.getItems();
    res.send(categories);
});

router.post('/', (req, res) => {

    if (req.body.name.length === 0) {
        const error = {"error": "Category name must be present in the request"};
        return res.status(400).send(error);
    }
    const result = categoriesDb.addItem(req.body);
    res.send(result);
});

router.get('/:id', (req, res) => {

    const result = categoriesDb.getItemById(req.params.id);
    res.send(result);
});

router.delete('/:id', (req, res) => {

    const result = categoriesDb.deleteItemById(req.params.id);
    if(result === 'deleted') {
        res.send(result);
    } else if (result === 'foreign key error') {
        const error = {"error": "this category refers from another table u cant delete !: " + req.params.id};
        return res.status(400).send(error);
    } else {
        const error = {"error": "there is no category with ID: " + req.params.id};
        return res.status(400).send(error);
    }
});

module.exports = router