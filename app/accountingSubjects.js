const express = require('express');
const {nanoid} = require('nanoid');

const path = require('path');
const multer = require('multer');
const config = require('../config');
const accountingSubjectsDb = require('../fs/accountingSubjectsFs');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();


router.get('/', (req, res) => {

    const accountingSubjects = accountingSubjectsDb.getItems();
    res.send(accountingSubjects);
});

router.post('/', upload.single('image'), (req, res) => {

    const accountingSubject = req.body;

    if(req.file) {
        accountingSubject.image = req.file.filename;
    }

    if (accountingSubject.name.length === 0 || accountingSubject.categoryId === undefined || accountingSubject.locationId === undefined) {
        const error = {"error": "accountingSubject name, category id , location id must be present in the request"};
        return res.status(400).send(error);
    }
    const result = accountingSubjectsDb.addItem(req.body);
    res.send(result);
});

router.get('/:id', (req, res) => {

    const result = accountingSubjectsDb.getItemById(req.params.id);
    res.send(result);
});

router.delete('/:id', (req, res) => {

    const result = accountingSubjectsDb.deleteItemById(req.params.id);
    if(result === 'deleted') {
        res.send(result);
    } else {
        const error = {"error": "there is no accountingSubject with ID: " + req.params.id};
        return res.status(400).send(error);
    }
});

module.exports = router